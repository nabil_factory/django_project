from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.html import escape
from django.utils.text import slugify
from tasks.models import Collection
from tasks.models import Task


def index(request):
    context = dict()

    collection_slug = request.GET.get("collection")

    if not collection_slug:
        Collection.get_default_collection()
        return redirect(f"{reverse('tasks:home')}?collection=_defaut")

    collection = get_object_or_404(Collection, slug=collection_slug)

    context["collections"] = Collection.objects.order_by("slug")
    context["collection"] = collection
    context["tasks"] = collection.task_set.order_by("description")

    return render(request, "tasks/index.html", context=context)


def add_collection(request):
    # si l'utilisateur saisit des caractères spéciaux ou du code JavaScript
    # dans le champ "collection-name," ces caractères spéciaux seront échappés,
    # ce qui permettra de ne pas interpréter le code.
    collection_name = escape(request.POST.get("collection-name"))
    collection, created = Collection.objects.get_or_create(
        name=collection_name, slug=slugify(collection_name)
    )
    if not created:
        return HttpResponse("La collection existe déjà.", status=409)

    return render(request, "tasks/collection.html", context={"collection": collection})


def delete_collection(request, collection_pk):
    Collection.objects.get(pk=collection_pk).delete()
    return redirect("tasks:home")


def add_task(request):

    # request.POST contient 'collection' grâce à hx-vals
    try:
        collection = Collection.objects.get(slug=request.POST.get("collection"))
    except Collection.DoesNotExist:
        collection = Collection.get_default_collection()

    description = escape(request.POST.get("task-description"))
    task = Task.objects.create(description=description, collection=collection)

    return render(request, "tasks/task.html", context={"task": task})


def delete_task(request, task_pk):
    Task.objects.get(pk=task_pk).delete()
    return HttpResponse("")


def get_tasks(request, collection_pk):
    collection = get_object_or_404(Collection, pk=collection_pk)
    tasks = collection.task_set.order_by("description")

    return render(request, "tasks/tasks.html", context={"tasks": tasks, "collection": collection})
